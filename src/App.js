import React from 'react';
import { Provider } from 'react-redux';

import store from './store';

import ProductsListContainer from './products/ProductsListContainer';

const App = () => (
    <Provider store={store}>
        <ProductsListContainer />
    </Provider>
);

export default App;
