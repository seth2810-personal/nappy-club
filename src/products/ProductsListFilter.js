import { Row, Col, Button } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import React, { useState, useCallback, useEffect } from 'react';

import SelectList from '../layout/SelectList';

import {
    brandsSelector, brandsFilterSelector,
    categoriesSelector, categoriesFilterSelector
} from './selectors';
import { SetCategoriesFilter, SetBrandsFilter } from './actions';

import styles from './ProductsListFilter.module.scss';

const ProductsListFilter = ({
    triggerClose,
}) => {
    const dispatch = useDispatch();
    const brands = useSelector(brandsSelector);
    const categories = useSelector(categoriesSelector);
    const brandsSelected = useSelector(brandsFilterSelector);
    const categoriesSelected = useSelector(categoriesFilterSelector);

    const [ brandsValue, selectBrands ] = useState(brandsSelected);
    const [ categoriesValue, selectCategories ] = useState(categoriesSelected);

    const onApply = useCallback(() => {
        dispatch(SetCategoriesFilter(categoriesValue));
        dispatch(SetBrandsFilter(brandsValue));
        triggerClose();
    }, [ dispatch, brandsValue, categoriesValue, triggerClose ]);

    const onClear = useCallback(() => {
        dispatch(SetCategoriesFilter(null));
        dispatch(SetBrandsFilter(null));
        triggerClose();
    }, [ dispatch, triggerClose ]);

    useEffect(() => selectBrands(brandsSelected), [brandsSelected]);
    useEffect(() => selectCategories(categoriesSelected), [categoriesSelected]);

    return (
        <React.Fragment>
            <Row type="flex" gutter={24}>
                <Col md={12} xs={24}>
                    <SelectList
                        multiple
                        items={brands}
                        header="Бренды"
                        value={brandsValue}
                        onChange={selectBrands}
                    />
                </Col>
                <Col md={12} xs={24}>
                    <SelectList
                        multiple
                        header="Товары"
                        items={categories}
                        value={categoriesValue}
                        onChange={selectCategories}
                    />
                </Col>
            </Row>
            <Row type="flex" gutter={24}>
                <Col md={12} xs={24}>
                    <Button className={styles.actionBtn} onClick={onApply} block>Применить</Button>
                </Col>
                <Col md={12} xs={24}>
                    <Button className={styles.actionBtn} onClick={onClear} block>Сброс</Button>
                </Col>
            </Row>
        </React.Fragment>
    );
};

export default ProductsListFilter;
