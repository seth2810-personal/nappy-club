import { Row, Col } from 'antd';
import React, { useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import SelectList from '../layout/SelectList';

import { SortFields, SortOrders } from './constants';
import { SetSortingField, SetSortingOrder } from './actions';
import { sortFieldSelector, sortOrderSelector } from './selectors';

const mkItem = (label, value) => ({ label, value });

const ProductsListSorting = () => {
    const dispatch = useDispatch();
    const fieldSelected = useSelector(sortFieldSelector);
    const orderSelected = useSelector(sortOrderSelector);

    const sortFields = useMemo(() => [
        mkItem('Оценка', SortFields.RATING),
        mkItem('Кол-во оценок', SortFields.RATING_COUNT),
        mkItem('Цена', SortFields.PRICE),
    ], []);

    const sortOrders = useMemo(() => [
        mkItem('По возрастанию', SortOrders.ASC),
        mkItem('По убыванию', SortOrders.DESC),
    ], []);

    const onSortFieldChange = useCallback((value) => dispatch(SetSortingField(value)), [ dispatch ]);
    const onSortOrderChange = useCallback((value) => dispatch(SetSortingOrder(value)), [ dispatch ]);

    return (
        <React.Fragment>
            <Row type="flex" gutter={24}>
                <Col md={12} xs={24}>
                    <SelectList
                        header="Поле"
                        items={sortFields}
                        value={fieldSelected}
                        onChange={onSortFieldChange}
                    />
                </Col>
                <Col md={12} xs={24}>
                    <SelectList
                        header="Порядок"
                        items={sortOrders}
                        value={orderSelected}
                        onChange={onSortOrderChange}
                    />
                </Col>
            </Row>
        </React.Fragment>
    );
};

export default ProductsListSorting;
