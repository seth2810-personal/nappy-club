export const SortOrders = {
    ASC: true,
    DESC: false,
};

export const SortFields = {
    PRICE: 'price',
    RATING: 'rating',
    RATING_COUNT: 'ratingCount',
};
