import { List } from 'antd';
import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { listSelector } from './selectors';
import ProductsListItem from './ProductsListItem';

import styles from './ProductsList.module.scss';

const renderItem = (item, index) => (
    <ProductsListItem
        item={item}
        isComfort={index % 2 === 1}
    />
);

const ProductsList = () => {
    const items = useSelector(listSelector);

    const grid = useMemo(() => ({
        md: 2,
        sm: 1,
        gutter: 24,
    }), []);

    const pagination = useMemo(() => ({
        pageSize: 3,
        showLessItems: true,
        hideOnSinglePage: true,
        className: styles.pagination,
    }), []);

    const footer = useMemo(() => (
        <div className={styles.footer}>Всего {items.length}</div>
    ), [ items ]);

    return (
        <List
            grid={grid}
            footer={footer}
            dataSource={items}
            itemLayout="vertical"
            className={styles.list}
            pagination={pagination}
            renderItem={renderItem}
        />
    );
};

export default ProductsList;
