import { createSelector } from 'reselect'

const productsSelector = ({ products }) => products;

const uniq = (items) => [...new Set(items)];

const EMPTY_ARRAY = [];

const itemsSelector = createSelector(
    productsSelector,
    ({ items }) => items,
);

export const categoriesSelector = createSelector(
    itemsSelector,
    (items) => {
        const categories = items.map(({ category }) => category);
        return uniq(categories);
    },
);

export const brandsSelector = createSelector(
    itemsSelector,
    (items) => {
        const brands = items.map(({ brand }) => brand);
        return uniq(brands);
    },
);

const filterSelector = createSelector(
    productsSelector,
    ({ filter = {} }) => filter,
);

export const brandsFilterSelector = createSelector(
    filterSelector,
    ({ brands }) => brands || EMPTY_ARRAY,
);

export const categoriesFilterSelector = createSelector(
    filterSelector,
    ({ categories }) => categories || EMPTY_ARRAY,
);

const sortSelector = createSelector(
    productsSelector,
    ({ sort = {} }) => sort,
);

export const sortFieldSelector = createSelector(
    sortSelector,
    ({ field }) => field,
);

export const sortOrderSelector = createSelector(
    sortSelector,
    ({ order }) => order,
);

const filterItemsSelector = (items, brands, categories) => items.filter(({ brand, category }) => {
    if (brands.length > 0 && !brands.includes(brand)) {
        return false;
    }

    if (categories.length > 0 && !categories.includes(category)) {
        return false;
    }

    return true;
});

const createSortFn = (field, order) => (
    { [field]: left },
    { [field]: right }
) => ((left < right) ? -1 : (left > right) ? 1 : 0) * order;

const sortItemsSelector = (items, field, asc) => {
    if (typeof asc === 'undefined') {
        return items;
    } else if (typeof field === 'undefined') {
        return items;
    } else {
        return [...items].sort(createSortFn(field, asc ? 1 : -1));
    }
};

export const listSelector = createSelector(
    itemsSelector,
    brandsFilterSelector,
    categoriesFilterSelector,
    sortFieldSelector,
    sortOrderSelector,
    (items, brands, categories, field, order) => sortItemsSelector(
        filterItemsSelector(items, brands, categories),
        field, order,
    ),
);
