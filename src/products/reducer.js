import { handleActions, path } from 'create-redux-actions';

import data from './data.json';

import { SetBrandsFilter, SetCategoriesFilter, SetSortingField, SetSortingOrder } from './actions';

const items = data.map(({ rating, ratingCount, price, ...rest }) => ({
    ratingCount: Number(ratingCount),
    rating: Number(rating),
    price: Number(price),
    ...rest,
}));

const createSetFieldHandler = (name) => (state, { payload }) => ({
    ...state,
    [name]: payload,
});

export default handleActions({
    [path('filter')]: {
        [SetBrandsFilter]: createSetFieldHandler('brands'),
        [SetCategoriesFilter]: createSetFieldHandler('categories'),
    },
    [path('sort')]: {
        [SetSortingField]: createSetFieldHandler('field'),
        [SetSortingOrder]: createSetFieldHandler('order'),
    }
}, { items });
