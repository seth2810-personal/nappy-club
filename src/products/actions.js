import { createActions } from 'create-redux-actions';

export const SetSortingOrder = createActions('SetSortingOrder');
export const SetSortingField = createActions('SetSortingField');
export const SetBrandsFilter = createActions('SetBrandsFilter');
export const SetCategoriesFilter = createActions('SetCategoriesFilter');
