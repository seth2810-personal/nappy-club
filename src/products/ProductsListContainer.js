import React from 'react';
import { Row } from 'antd';

import Page from '../layout/Page';
import DropDownPopover from '../layout/DropDownPopover';

import ProductsList from './ProductsList';
import ProductsListFilter from './ProductsListFilter';
import ProductsListSorting from './ProductsListSorting';

const ProductsListContainer = () => (
    <Page title="Детские подгузники и трусики Nappyclub">
        <Row type="flex" justify="end">
            <DropDownPopover
                label="Сортировка"
                content={ProductsListSorting}
            />
            <DropDownPopover
                label="Фильтр"
                content={ProductsListFilter}
            />
        </Row>
        <ProductsList/>
    </Page>
);

export default ProductsListContainer;
