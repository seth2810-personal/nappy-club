import React from 'react';
import { List, Card, Rate, Row, Col, Button, Input } from 'antd';

import styles from './ProductsListItem.module.scss';

const PiecesInput = () => (
    <Input.Group className={styles.piecesInput}>
        <div className="ant-input-group-addon">
            <Button icon="minus" disabled/>
        </div>
        <Input className={styles.piecesInput} value="1"/>
        <div className="ant-input-group-addon">
            <Button icon="plus" disabled/>
        </div>
    </Input.Group>
);

const ProductsListItem = ({ item, image, isComfort }) => (
    <List.Item className={isComfort ? styles.itemComfort : styles.itemPremium}>
        <Card className={styles.card}>
            <Row gutter={16}>
                <Col md={8} sm={24}>
                    <img className={styles.label} alt=""/>
                    <div className={styles.image}>
                        <img alt={item.title}/>
                    </div>
                    <div className={styles.ratingMeta}>
                        <Rate
                            className={styles.rating}
                            value={+item.rating}
                            count={5}
                            disabled
                        />
                        <span className={styles.ratingCount}>
                            ({item.ratingCount})
                        </span>
                    </div>
                </Col>
                <Col className={styles.meta} md={16}>
                    <div className={styles.name}>{item.title}</div>
                    <div className={styles.size}>Кол-во в пачке: {item.ratingCount} шт</div>
                    <div className={styles.price}>Цена: {item.price} ₽</div>
                    <div className={styles.actions}>
                        <div className={styles.piecesInputContainer}>
                            <PiecesInput/>
                        </div>
                        <Button className={styles.buyBtn}>Купить</Button>
                    </div>
                </Col>
            </Row>
        </Card>
    </List.Item>
);


export default ProductsListItem;
