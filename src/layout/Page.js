import React from 'react';
import { Layout, Row, Col } from 'antd';

import styles from './Page.module.scss';

const Page = ({ title, children }) => (
    <Row type="flex" justify="center" className={styles.page}>
        <Col span={16}>
            <Layout className={styles.layout}>
                <Layout.Header className={styles.header} children={title} />
                <Layout.Content className={styles.content} children={children}/>
            </Layout>
        </Col>
    </Row>
);

export default Page;
