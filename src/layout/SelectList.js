import { List, Checkbox, Radio } from 'antd';
import React, { useCallback, useMemo } from 'react';

import styles from './SelectList.module.scss';

const SelectList = ({
    items,
    header,
    onChange,
    value = null,
    size = 'small',
    multiple = false,
}) => {
    const renderer = useCallback((item) => {
        const isObject = typeof item === 'object';
        const Component = multiple ? Checkbox : Radio;
        const { label, value } = isObject ? item : { label: item, value: item };

        return (
            <List.Item className={styles.item}>
                <Component value={value} children={label} />
            </List.Item>
        );
    }, [ multiple ]);

    const onSelectChange = useCallback((event) => {
        onChange(multiple ? event : event.target.value);
    }, [ multiple, onChange ]);

    const Group = useMemo(() => multiple
        ? Checkbox.Group
        : Radio.Group
    , [ multiple ]);

    return (
        <Group
            value={value}
            className={styles.group}
            onChange={onSelectChange}
        >
            <div className={styles.header} children={header} />
            <List
                size={size}
                split={false}
                dataSource={items}
                renderItem={renderer}
                className={styles.list}
            />
        </Group>
    );
};

export default SelectList;
