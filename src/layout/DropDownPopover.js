import { Button, Icon, Dropdown } from 'antd';
import React, { useState, useCallback } from 'react';

import styles from './DropDownPopover.module.scss';

const DropDownPopover = ({
    label,
    content: Content,
}) => {
    const [visible, setVisible] = useState(false);
    const open = useCallback(() => setVisible(true), [setVisible]);
    const close = useCallback(() => setVisible(false), [setVisible]);

    const overlay = (
        <div className={styles.container}>
            <Icon
                className={styles.close}
                type="close-circle"
                onClick={close}
            />
            <Content triggerClose={close} />
        </div>
    );

    return (
        <Dropdown
            overlay={overlay}
            visible={visible}
            trigger={["click"]}
            placement="bottomRight"
            onVisibleChange={setVisible}
        >
            <Button className={styles.trigger} onClick={open} children={label}/>
        </Dropdown>
    );
};

export default DropDownPopover;
