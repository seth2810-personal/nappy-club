This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Description

1. Сверстать макет
2. Используя данные в json, подставить их в шаблон
3. На react написать логику на обработку событий
4. Сверстать и написать обработчики событий на фильтрацию контента

Пагинация.

1. Необходимо вывести первые 3 товара из data.json, разбив на страницы (на макете отображено больше товаров, это игнорируем)
2. На одной странице отображается по 3 товара.
3. Сформировать пагинатор (внизу страницы)
4. Учитываем, что при клике на другую страницу примененные фильтры не должны сбрасываться.
5. Отобразить общее кол-во товаров

Фильтрация и сортировка.

1. У товара есть параметры: название (title), бренд (brand), категория (category), оценка (rating 1-5), кол-во оценок (ratingCount), цена (price), наличие на складе (stock: 1||0, предзаказ||купить).
2. Необходимо сделать фильтры по параметру: категория, бренд (возможны оба одновременно).
3. Необходимо сделать сотрировку по параметру: оценка, кол-во оценок,цена (убывание/возрастание).
4. При применении фильтра/сортировки — обновляем товары, учитывая пагинацию.
5. Нажимая на “Сбросить фильтры” — выводим все товары без фильтров, учитывая пагинацию и сортировку.
6. Обновить отображаемое общее кол-во товаров (после применения фильтра)

Результат выложить в репо и возможность запустить результат на просмотр.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
